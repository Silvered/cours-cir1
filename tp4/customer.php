<?php
function printEventsCustomerCalendar($date){//if we have under 5minutes.
  $ini_array = parse_ini_file("secrets.ini",true);
  try {
     $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
     $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
  } catch (Exception $e) {
          exit("We can't connect you to the database");
  }

  $query = "SELECT * FROM events";
  $preparation=$bdd->prepare($query);
  $preparation->execute();
  $actualTimes=strtotime($date);
  $j=1;
  foreach($preparation as $row) {
    $startTimes=strtotime($row["startdate"])-3600*24;
    $endTimes=strtotime($row["enddate"]);
    if($startTimest<=$actualTimes&&$actualTimes<=$endTimes){
      if($j<=5&&$row["nb_place"]>0){
        $j+=1;
        echo "<form action='controller.php' method='POST'>";
        echo "<input type='hidden' value='".htmlspecialchars($row["id"])."' id='eventId' name='eventId'/>";
        echo "<input type='submit' value='".htmlspecialchars($row["name"])."' name='book'/>";
        echo "</form>";
      }
      else if($j== 6){
        echo "<form method='POST' action='controller.php'>";
        echo "<input type='hidden' value=".$date." name='date' id='date'/>";
        echo "<input type='submit' name='showMore' value='show more'/>";
        echo "</form>";
      }
    }
  }
}

function printEventsCustomerAll($date){
  $ini_array = parse_ini_file("secrets.ini",true);
  try {
     $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
     $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
  } catch (Exception $e) {
          exit("We can't connect you to the database");
  }
  $query = "SELECT * FROM events";
  $preparation = $bdd->prepare($query);
  $preparation->execute();
  $actualTimes=strtotime($date);
  foreach($preparation as $row) {
    $startTimes=strtotime($row["startdate"])-3600*24;
    $endTimes=strtotime($row["enddate"]);
    if($startTimes<=$actualTimes&&$actualTimes<=$endTimes){
      if($row["nb_place"]>0){
        echo "<tr>";
        echo "<td>".$row["name"]."</td>";
        echo "<td>".$row["description"]."</td>";
        echo "<td>".htmlspecialchars(date("Y-m-d", strtotime($row["startdate"])))."</td>";
        echo "<td>".htmlspecialchars(date("Y-m-d", strtotime($row["enddate"])))."</td>";
        echo "<td>".htmlspecialchars($row["nb_place"])."</td>";
        echo "<td>";
         echo "<form action='controller.php' method='POST'>";
         echo "<input type='hidden' value='".htmlspecialchars($row["id"])."' id='eventId' name='eventId'/>";
         echo "<input type='submit' value='Reserved' name='go'/>";
         echo "</form>";
        echo "</td>";
        echo "</tr>";
      }
    }
  }
}

function printBook($id){
  $ini_array = parse_ini_file("secrets.ini",true);
  try {
     $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
     $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
  } catch (Exception $e) {
          exit("We can't connect you to the database");
  }

  $query = "SELECT * from events WHERE id= :id";
  $statements = $bdd->prepare($query);
  $statements->execute([":id" => $id]);
  foreach ($statements as $row) {
    if($row["nb_place"] > 0){
      echo "<tr>";
      echo "<td>".htmlspecialchars($row["name"])."</td>";
      echo "<td>".htmlspecialchars($row["description"])."</td>";
      echo "<td>".htmlspecialchars(date("Y-m-d", strtotime($row["startdate"])))."</td>";
      echo "<td>".htmlspecialchars(date("Y-m-d", strtotime($row["enddate"])))."</td>";
      echo "<td>".htmlspecialchars($row["nb_place"])."</td>";
      echo "<td>";
       echo "<form action='controller.php' method='POST'>";
       echo "<input type='hidden' value='".htmlspecialchars($row["id"])."' id='eventId' name='eventId'/>";
       echo "<input type='submit' value='Book' name='go'>";
       echo "</form>";
      echo "</td>";
      echo "</tr>";
    }
  }
}

function myIncEvents(){
    $ini_array = parse_ini_file("secrets.ini",true);
  try {
     $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
     $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
  } catch (Exception $e) {
          exit("We can't connect you to the database");
  }

  $query = "SELECT * FROM events INNER JOIN user_participates_events ON user_participates_events.id_event = events.id WHERE user_participates_events.id_participant = :id";
  $preparation = $bdd->prepare("$query");
  $preparation->execute([":id" => $_SESSION["id"]]);
  foreach($preparation as $row){
      echo "<tr>";
      echo "<td>".htmlspecialchars($row["name"])."</td>";
      echo "<td>".htmlspecialchars($row["description"])."</td>";
      echo "<td>".htmlspecialchars(date("Y-m-d", strtotime($row["startdate"])))."</td>";
      echo "<td>".htmlspecialchars(date("Y-m-d", strtotime($row["enddate"])))."</td>";
      echo "</tr>";
  }
}


if(filter_input(INPUT_POST, "home", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["showmore"] = NULL;
  $_SESSION["eventBook"] = NULL;
  header("Location: index.php");
}
if(filter_input(INPUT_POST, "book", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["eventBook"] = filter_input(INPUT_POST, "eventId", FILTER_SANITIZE_SPECIAL_CHARS);
  header("Location: index.php");
}


if(isset($_POST["come"])){
  $query = "INSERT INTO user_participates_events (id_participant, id_event) VALUES(:id_participant, :id_events)";
  $preparation = $bdd->prepare($query);
  $preparation->execute([":id_participant" => $_SESSION['id'], ":id_events" => $_POST['eventId']]);
  $query = "SELECT nb_place FROM events where id= :id";
  $preparation = $bdd->prepare($query);
  $preparation->execute([":id" => $_POST["eventId"]]);
  foreach ($preparation as $row) {
    $nbPlace = $row["nb_place"];
  }
  $query = "UPDATE events SET nb_place = :nbPlace WHERE id = :id";
  $preparation = $bdd->prepare($query);
  $preparation->execute([":nbPlace" => $nbPlace-1, ":id" => $_POST["eventId"]]);
  $_SESSION["showmore"] = NULL;
  $_SESSION["eventBook"] = NULL;
  header("Location: index.php");
}


if(isset($_POST["myEvents"])){
    $_SESSION["loadingpage"] = "myEvents";
}
?>