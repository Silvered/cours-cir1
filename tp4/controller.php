<?php
$ini_array = parse_ini_file("secrets.ini",true);
session_start();

try {
   $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
   $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
} catch (Exception $e) {
        exit("We can't connect you to the database.");
}


if(filter_input(INPUT_POST, "submit", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["rank"] = NULL;
  $_SESSION["log"] = NULL;
  $_SESSION["id"] = NULL;
  if(filter_input(INPUT_POST, "login", FILTER_SANITIZE_SPECIAL_CHARS)){
    $login =filter_input(INPUT_POST, "login", FILTER_SANITIZE_SPECIAL_CHARS);
    if(filter_input(INPUT_POST, "password", FILTER_SANITIZE_SPECIAL_CHARS)){
      $password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_SPECIAL_CHARS);
      try{
        $query = "SELECT * FROM Users WHERE login = :log";
        $preparation = $bdd->prepare($query);
        $preparation->execute([":log" => $login]);
        foreach ($preparation as $row) {
          if(password_verify($password, $row["password"])){;
            $_SESSION["rank"] = $row["rank"];
            $_SESSION["id"] = $row["id"];
            $_SESSION["log"] = $row["login"];
            $_SESSION["date"] = date("Y-m-d");
          }
        }
      }catch(Exception $e){
        exit("We can't connect you, the password is probably wrong");
      }
    }
  }
}

//If you push log out
if(filter_input(INPUT_POST, "log out", FILTER_SANITIZE_SPECIAL_CHARS)){
  session_destroy();
}


//For select a month.
if(filter_input(INPUT_POST, "previousMonth", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["date"] = date("Y-m-d", strtotime($_SESSION["date"]." -1 month"));
header("Location: index.php");
}
if(filter_input(INPUT_POST, "nextMonth", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["date"] = date("Y-m-d", strtotime($_SESSION["date"]." +1 month"));
header("Location: index.php");
}


include("customer.php");
include("organizer.php");
function printCalendar($date){
  $month=date('M', strtotime($date));
  $year=date("Y", strtotime($date));
  $firstDay=date("l", strtotime($month."-".$year));
  $lastDay=date("t", strtotime($date));
  $fday = date("N", strtotime($firstDay));
  for($i = 1; $i<$lastDay+$fday; $i++){
    if($i % 7 == 1){
      echo "<tr>";
    }
    else if($i % 7 == 7){
      echo "</tr>";
    }
    if($i<$fday){
      echo "<td>";
    }
    else if($i){
      echo "<td>";
      echo "<div>".htmlspecialchars(date("d", strtotime($i-$fday+1 ."-".$month."-".$year)))."</div>";
      if($_SESSION["rank"] == "CUSTOMER"){
        printEventsCustomerCalendar($i-$fday+1 ."-".$month."-".$year);
      }
      if($_SESSION["rank"] == "ORGANIZER"){
        printEventsOrganizerCalendar($i-$fday+1 ."-".$month."-".$year);
      }
      echo "</td>";
    }
  }
  while($i%7 != 1){
      echo "<td>";
      echo "</td>";
      $i++;
  }
}

