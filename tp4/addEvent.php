<header> .
    <form method="POST" action="controller.php" >
        <input type="submit" value="Log out" name="log out" />
        <input type="submit" value="Home" name="home"  />
        <input type="submit" value="Add an event" name="addEvent"/>
    </form>
</header>
    <h1>Add an event</h1>
    <form action="controller.php" method="POST" enctype="multipart/form-data">
        <input type="text" name="eventName" placeholder="event's name"/>
        <input type="text" name="desc" placeholder="event's description"/>
        <label for="startdate">Start date:</label>
        <input type="date" name="startdate" id="startdate"/>
        <label for="starttime">Start time:</label>
        <input type="time" name="starttime" id="starttime"/>
        <label for="enddate">end date:</label>
        <input type="date" name="enddate" id="enddate"/>
        <label for="endtime">end time:</label>
        <input type="time" name="endtime" id="endtime"/>
        <label for="places">Number of places:</label>
        <input type="number" name="places" id="places"/>
        <input type="submit" name="submitEvent"/>
    </form>
