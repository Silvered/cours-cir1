<!DOCTYPE HTML>
    <html>
 <head>
  <meta charset="utf-8"/>
  <title> Event Calendar </title>
 </head>
 <body>
  <?php
     if(isset($_SESSION["rank"], $_SESSION["log"])){
      echo $_SESSION["log"];
      if($_SESSION["rank"] == "ORGANIZER"){
        if(isset($_SESSION["loadingpage"])){
          if($_SESSION["loadingpage"] == "showMore"){
            include("showall.php");
          }
          else if($_SESSION["loadingpage"] == "addEvent"){
                  include("addEvent.php");
              }
        }
        else if(isset($_SESSION["eventBook"])){
          include("book.php");
        }
        else{
          include("host.php");
        }
      }
      else if($_SESSION["rank"] == "CUSTOMER"){
          if(isset($_SESSION["loadingpage"])){
              if($_SESSION["loadingpage"] == "myEvents"){
                  include("myevents.php");
              }
              else if($_SESSION["loadingpage"] == "showMore"){
                  include("showall.php");
              }
          }
          else if(isset($_SESSION["eventBook"])){
              include("book.php");
          }
          else{
              include("client.php");
          }
      }
     } 
    else{
      include("form.php");
    }
   
  ?>
 </body>
</html>
