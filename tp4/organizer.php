<?php
function printEventsOrganizerCalendar($date){
  $ini_array = parse_ini_file("secrets.ini",true);
  try {
     $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
     $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
  } catch (Exception $e) {
          exit("We can't connect you to the database");
  }


  $query = "SELECT * FROM events";
  $preparation = $bdd->prepare($query);
  $preparation->execute();
  $actualTimes = strtotime($date."+1");
  $j=1;
  foreach($preparation as $row) {
    $startTimes = strtotime($row["startdate"])-3600*24;
    $endTimes = strtotime($row["enddate"]."+2");
    if($startTimes <= $actualTimes && $actualTimes <= $endTimes){
      if($j<= 5 && $row["organizer_id"] == $_SESSION["id"]){
        $j+=1;
        echo "<form action='controller.php' method='POST'>";
        echo "<input type='hidden' value='".htmlspecialchars($row["id"])." name='eventId'/>";
        echo "<input type='submit' value='".htmlspecialchars($row["name"])." name='cancel'/>";
        echo "</form>";


      }
      else if($j== 6){
        echo "<form method='POST' action='controller.php'>";
        echo "<input type='hidden' value=".htmlspecialchars($date)." name='date' id='date'/>";
        echo "<input type='submit' name='showMore' value='show more'/>";
        echo "</form>";
      }
    }
  }
}

function printEventsOrganizerAll($date){
  $ini_array = parse_ini_file("secrets.ini",true);
  try {
     $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
     $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
  }
  catch (Exception $e) {
          exit("We can't connect you to the database");
  }



  $query = "SELECT * FROM events";
  $preparation= $bdd->prepare($query);
  $preparation->execute();
  $actualTimes=strtotime($date);
  foreach($preparation as $row) {
    $startTimes=strtotime($row["startdate"])-3600*24;
    $endTimes= strtotime($row["enddate"]);
    if($startTimes <= $actualTimes && $actualTimes <= $endTimes){
      echo "<tr>";
      echo "<td>".htmlspecialchars($row["name"])."</td>";
      echo "<td>".htmlspecialchars($row["description"])."</td>";
      echo "<td>".htmlspecialchars(date("Y-m-d", strtotime($row["startdate"])))."</td>";
      echo "<td>".htmlspecialchars(date("Y-m-d", strtotime($row["enddate"])))."</td>";
      echo "<td>".htmlspecialchars($row["nb_place"])."</td>";
      echo "<td>";
      echo "<form action='controller.php' method='POST'>";
      echo "<input type='hidden' value='".$row["id"]."' id='eventId' name='eventId'/>";
      echo "<input type='submit' value='Cancel' name='cancel'/>";
      echo "</form>";
      echo "</td>";
      echo "</tr>";
    }
  }
}


if(filter_input(INPUT_POST, "submitEvent", FILTER_SANITIZE_SPECIAL_CHARS)){
    if(filter_input(INPUT_POST, "eventName", FILTER_SANITIZE_SPECIAL_CHARS)){
        $name = filter_input(INPUT_POST,"eventName", FILTER_SANITIZE_SPECIAL_CHARS);
        if(filter_input(INPUT_POST, "desc", FILTER_SANITIZE_SPECIAL_CHARS)){
            $desc = filter_input(INPUT_POST,"desc", FILTER_SANITIZE_SPECIAL_CHARS);
            if(filter_input(INPUT_POST, "startdate", FILTER_SANITIZE_SPECIAL_CHARS) && filter_input(INPUT_POST, "starttime", FILTER_SANITIZE_SPECIAL_CHARS)){
                $startdate = filter_input(INPUT_POST, "startdate", FILTER_SANITIZE_SPECIAL_CHARS);
                $starttime = filter_input(INPUT_POST, "starttime", FILTER_SANITIZE_SPECIAL_CHARS);
                $startdate = $startdate." ".$starttime;
                if(filter_input(INPUT_POST, "enddate", FILTER_SANITIZE_SPECIAL_CHARS) && filter_input(INPUT_POST, "endtime", FILTER_SANITIZE_SPECIAL_CHARS)){
                    $enddate = filter_input(INPUT_POST, "enddate", FILTER_SANITIZE_SPECIAL_CHARS);
                    $endtime = filter_input(INPUT_POST, "endtime", FILTER_SANITIZE_SPECIAL_CHARS);
                    $enddate = $enddate." ".$endtime;
                    if(filter_input(INPUT_POST, "places", FILTER_SANITIZE_SPECIAL_CHARS)){
                      $places = filter_input(INPUT_POST,"places", FILTER_SANITIZE_SPECIAL_CHARS);
                          $query = "INSERT INTO events (name,description,startdate,enddate, organizer_id, nb_place) VALUES(:name, :desc, :startdate, :enddate, :id, :places)";
                          $preparation = $bdd->prepare($query);
                          $preparation->execute([":name"=>$name, ":desc"=>$desc,":startdate"=>$startdate, ":enddate"=>$enddate,":id"=>$_SESSION["id"],":places"=>$places]);
                          $query = "SELECT * FROM events WHERE name = :name AND description = :desc";
                          $preparation2 = $bdd->prepare($query);
                          $preparation2->execute([":name" => $name, ":desc"=>$desc]);
                          foreach($preparation2 as $row){
                            $id = $row["id"];
                          }
                          $_SESSION["loadingpage"] = NULL;
                          header("Location: index.php");
                        }
                    }
                }
            }
        }
    }



if(filter_input(INPUT_POST, "addEvent", FILTER_SANITIZE_SPECIAL_CHARS)){
    $_SESSION["loadingpage"] = "addEvent";
    header("Location: index.php");
}

if(filter_input(INPUT_POST, "cancel", FILTER_SANITIZE_SPECIAL_CHARS)){
    $id = filter_input(INPUT_POST, "eventId", FILTER_SANITIZE_SPECIAL_CHARS);
    $query = "DELETE FROM user_participates_events WHERE id_event = :id";
    $preparation2 = $bdd->prepare($query);
    $preparation2->execute([":id"=>$id]);
    $query = "DELETE FROM events WHERE id = :id";
    $preparation = $bdd->prepare($query);
    $preparation->execute([":id"=>$id]);
    header("Location: index.php");
}

