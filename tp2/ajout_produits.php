<!DOCTYPE html>
<html>
    <head>
        <title> Epicerie </title>
        <meta charset="utf-8"/> 
    </head>   
    
    <body>
        <form action="recup.php" method="POST" enctype="multipart/form-data">
            <fieldset id="identity">
            <p>
                <input type="text" name="nom" id="nom"/><label for="nom">Nom du produit</label>
            </p>
            <p>
                <input type="file" name="photo" id="photo"/><label for="photo">La photo de votre produit</label>
            </p>
            <p>
                <input type="number" min="0.01" step="0.01" name="prix" id="prix"/><label for="prix">Prix</label>                
            </p>
            <p>
                <input type="number" min="0.01" step="0.01" name="qte" id="qte"/><label for="qte">Quantité du Produit</label>
            </p>
            </fieldset>
          <p>
              <input type="submit" name="ajout" value="Ajouer un produit"/>
          </p>
        </form>
    </body>
    
</html>